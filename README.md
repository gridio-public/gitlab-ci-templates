# Templates for Gitlab CI / CD pipelines

Most of the CI/CD requirements are similar, therefore we are using these YAML files as underlying
definition for pipelines.

* Module repo init just initialises the pipeline with proper credentials, have it always as the first line of your .gitlab-ci.yml
* Infrastructure template is meant for terraform build pipelines
* Service CI template is generic build pipeline for a microservice
* Microservice template (microservice-template.yml) defines the build step of the pipeline from one line above. This is a generic Docker build, so
if you need something that is more specific, like add some lambdas or more than one docker image, then you need
to write a bespoke build script and not use this template.